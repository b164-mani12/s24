//2 $or operator ,$regex and field projection 
db.users.find({$or: [ {firstName:  { $regex: 's', $options: '$i'}} , {lastName:  { $regex: 'd', $options: '$i'}}  ]},{
	    
	 
	    _id: 0,
	    lastName: 1,
	    firstName: 1

	    
	    })

//3 $and operator, &gte
db.users.find({ $and: [ {department: "HR"}, {age: {$gte : 70} } ] })

//4 $and, $regex and $lte operators
db.users.find({ $and: [ {firstName:  { $regex: 'e', $options: '$i'}}, {age: {$lte : 30} } ] })